package ictgradschool.industry.lab14.ex02;

import org.apache.commons.lang3.concurrent.ConcurrentException;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.*;

/**
 * Simple application to calculate the prime factors of a given number N.
 * <p>
 * The application allows the user to enter a value for N, and then calculates
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 */
public class MultiThreadedPrimeApp extends JPanel {

    private JButton _startBtn;        // Button to start the calculation process.
    private JButton _cancelBtn;       // Cancel Button
    private JTextArea _factorValues;  // Component to display the result.
    private PrimeFactorisationWorker primeWorker; // prime worker

    public MultiThreadedPrimeApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        final JTextField tfN = new JTextField(20);
        _startBtn = new JButton("Compute");
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);

        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String strN = tfN.getText().trim();
                long n = 0;

                try {
                    n = Long.parseLong(strN);
                } catch (NumberFormatException e) {
                    System.out.println(e);
                }

                // Disable the Start button until the result of the calculation is known.
                _startBtn.setEnabled(false);

                // Clear any text (prime factors) from the results area.
                _factorValues.setText(null);

                // Set the cursor to busy.
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                //Long Running Code
                // Start the computation in the Event Dispatch thread.
//                for (long i = 2; i * i <= n; i++) {
//
//                    // If i is a factor of N, repeatedly divide it out
//                    while (n % i == 0) {
//                        _factorValues.append(i + "\n");
//                        n = n / i;
//                    }
//                }
//
//                // if biggest factor occurs only once, n > 1
//                if (n > 1) {
//                    _factorValues.append(n + "\n");
//                }

                // Re-enable the Start button.
                _startBtn.setEnabled(false);
                _cancelBtn.setEnabled(true);
                // Restore the cursor.
                setCursor(Cursor.getDefaultCursor());

                primeWorker = new PrimeFactorisationWorker(n);
                primeWorker.execute();

            }
        });

        _cancelBtn = new JButton("Cancel");
        _cancelBtn.setEnabled(false);

        _cancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                primeWorker.cancel(true);
            }
        });

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);
        controlPanel.add(_cancelBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500, 300));
    }

    private class PrimeFactorisationWorker extends SwingWorker<List<Long>, Long> {
        private long n;

        PrimeFactorisationWorker(long n) {
            this.n = n;
        }

        @Override
        protected List<Long> doInBackground() throws Exception {

            List<Long> factorList = new ArrayList<>();

            for (long i = 2; i * i <= n; i++) {

                if (isCancelled()) {
                    return null;
                }

                // If i is a factor of N, repeatedly divide it out
                while (n % i == 0) {
//                    _factorValues.append(i + "\n");
                    factorList.add(i);
                    publish(i);
                    n = n / i;
                }

            }

            // if biggest factor occurs only once, n > 1
            if (n > 1) {
//                _factorValues.append(n + "\n");
                factorList.add(n);
                publish(n);
            }

            return factorList;
        }

        @Override
        protected void process(List<Long> chunks) {
            for (Long value: chunks){
                _factorValues.append(value + "\n");
            }
        }

        @Override
        protected void done() {

            try {
                List<Long> values = get();
            }

            catch (CancellationException e) {
                _factorValues.append("Cancelled");
            }

            catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                _factorValues.append("Error: " + e.getMessage());
            }

            // Re-enable the Start button.
            _startBtn.setEnabled(true);
            _cancelBtn.setEnabled(false);

            // Restore the cursor.
            setCursor(Cursor.getDefaultCursor());
        }
    }
    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new MultiThreadedPrimeApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}